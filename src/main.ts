import {
  app,
  BrowserWindow,
  ipcMain,
  IpcMainEvent,
  webContents,
} from "electron";
import * as path from "path";

//import { createCharacterMain } from './create-character-main';
import { cmd } from "./mswNotUsed/cmd";

let mainWin: BrowserWindow = null;

let bistroMain = () => {
  mainWin = new BrowserWindow({
    width: 375,
    height: 667,
    show: false,
    webPreferences: { nodeIntegration: true },
  });
  mainWin.webContents.openDevTools();
  mainWin.loadFile(path.join(__dirname, "../mountainSeaWorldMainPage.html"));
  mainWin.on("ready-to-show", () => {
    mainWin.show();
  });
  mainWin.on("closed", () => {
    mainWin = null;
  });

};

app.on("ready", bistroMain);
app.on("window-all-closed", () => {
  if (process.platform !== "darwin") {
    app.quit();
  }
});

app.on("activate", () => {
  if (mainWin === null) {
    bistroMain;
  }
});
