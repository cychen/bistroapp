
import { confirmationNewCharacter } from "./msw-confirmation-new-character-render";
import { mswPlayerMap } from "./msw-player-map";
import { inputElements } from "./mswElements/input-elements";

//1. get name map from storageTextMap
//2. check name in name map
//  yes => log in success
//  no => create new char

let checkLogin = async () => {
  let loginName = inputElements.getLoginInputValue();
  let nameMap: Map<string, string> = await (mswPlayerMap.getTextMap('name'));
  for (let [id, name] of nameMap) {
    if (name === loginName) {
      console.log(`login success`)
      inputElements.enableLoginInput();
      inputElements.defaultLoginInputValue();
      //start-game(name, id)
      return;
    }
  }
  console.log(`charName not found`)
  confirmationNewCharacter();
  return;
}



export { checkLogin };
