
import { UIElement } from "./ui-element";
import { gameText } from "./msw-game-text";
import { mswPlayerMap } from "./msw-player-map";
import { roomElementContainers } from "./mswElementContainers/room-element-containers";
import { roomElements } from "./mswElements/room-elements";
import { inputElements } from "./mswElements/input-elements";
import { inputElementContainers } from "./mswElementContainers/input-element-containers";
import { newCharLs } from "./mswInputLs/newCharLs";

//import { renderGamePage } from "./msw-game-page";

let roomBody: UIElement;
let roomBodyContainer: UIElement;
let newCharTextElement: UIElement;
let loginInput: UIElement;
let newCharInput: UIElement;
let inputContainer: UIElement;
let inputName: string;
let newCharId: string;

let roomBodyWelcomeReplacedWithNewCharMessage = () => {
  let newCharName = inputElements.getLoginInputValue();
  let newCharMessage = gameText.createNewCharText(newCharName);
  newCharTextElement = new UIElement('text');
  newCharTextElement.setTextContent(newCharMessage);
  roomBody = roomElements.roomBody;
  roomBodyContainer = roomElementContainers.roomBodyContainer;
  roomBodyContainer.node.removeChild(roomBody.node);
  roomBodyContainer.node.appendChild(newCharTextElement.node)
}

let createActionBtn = () => {
  //create new char?
  // yes =>
  //  update nameMap, locationMap, moneyMap
  //   a: add <id, newName> to player name map
  //   b: update storageTextMap <name, nameMap>
  //   c: add <id, roomId> to player location map
  //   d: update storageTextMap <location, locationMap>
  //   e: start game page
  // no =>
  //   back to welcome page

  updatePlayerNameMap();
  updatePlayerLocationMap();
  updatePlayerMoneyMap();
  // renderGamePage();

}

let updatePlayerNameMap = async () => {
  let nameMap: Map<string, string> = await mswPlayerMap.getTextMap(`name`);
  newCharId = `Name` + `${nameMap.size + 1}`;
  nameMap.set(newCharId, inputName);
  mswPlayerMap.updateTextMap(`name`, nameMap);
}

let updatePlayerLocationMap = async () => {
  let locationMap: Map<string, string> = await mswPlayerMap.getTextMap(`location`);
  locationMap.set(newCharId, `Room1`);
  mswPlayerMap.updateTextMap(`location`, locationMap);
}

let updatePlayerMoneyMap = async () => {
  let moneyMap: Map<string, number> = await (mswPlayerMap.getValueMap('money'));
  moneyMap.set(newCharId, 0);
  mswPlayerMap.updateValueMap('money', moneyMap);
}


let inputLoginReplacedWithNewChar = () => {
  loginInput = inputElements.loginInput;
  inputContainer = inputElementContainers.inputContainer;
  newCharInput = inputElements.newCharInput;
  inputElements.defaultNewCharInputValue();
  inputContainer.node.removeChild(loginInput.node);
  inputContainer.node.appendChild(newCharInput.node);
}

let listenToNewCharInput = () => {
  newCharInput.node.addEventListener('change', newCharLs);
}

let confirmationNewCharacter = () => {
  roomBodyWelcomeReplacedWithNewCharMessage();
  inputLoginReplacedWithNewChar();
  listenToNewCharInput();
}


export { confirmationNewCharacter };
