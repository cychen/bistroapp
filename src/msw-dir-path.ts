import * as path from "path";

class MswDirPath {
  private playerDirPath = path.join(__dirname, "../../mswPlayer");
  private playerTextMapDirPath = `${this.playerDirPath}/textMap`;
  private playerValueMapDirPath = `${this.playerDirPath}/valueMap`;
  private playerKeyVsTypeMapDirPath = `${this.playerDirPath}/keyVsTypeMap`;
  private roomDirPath = path.join(__dirname, "../../mswRoom");
  private roomTextMapDirPath = `${this.roomDirPath}/textMap`;
  private roomExitMapDirPath = `${this.roomDirPath}/exitMap`; 

  getPlayerDir = (): string =>{
    return this.playerDirPath;
  }

  getPlayerTextMapDir = (): string => {
    return this.playerTextMapDirPath;
  }

  getPlayerValueMapDir = (): string => {
    return this.playerValueMapDirPath;
  }

  getPlayerKeyVsTypeMapDirPath = (): string => {
    return this.playerKeyVsTypeMapDirPath;
  }

  getRoomDir = (): string => {
    return this.roomDirPath;
  }

  getRoomTextMapDir = (): string => {
    return this.roomTextMapDirPath;
  }

  getRoomExitMapDir = (): string => {
    return this.roomExitMapDirPath;
  }
}

let mswDirPath = new MswDirPath();
export { mswDirPath };