/*
import { styleLabel } from "./mswStyle/style-label";
import { mswUITextArea } from "./msw-ui-textarea";
import { mswUIInput } from "./msw-ui-input";
import { mswUIBtn } from "./msw-ui-btn";
import { mswUIContainer } from "./msw-ui-container";
import { UIElement } from "./ui-element";

  let loginInputElement: UIElement;
  let gameInputElement: UIElement;
  let yesBtn: UIElement;
  let noBtn: UIElement;
  let pageTitleTextArea: UIElement;
  let pageBodyTextArea: UIElement;
  let pageContainer: UIElement;
  let pageTitleContainer: UIElement;
  let pageBodyContainer : UIElement;
  let pageInputContainer: UIElement;
  let pageBtnContainer: UIElement;
  
  let getGamePageUIElements = ()=> {
    getGameTitleTextElement();
    getMainBodyTextElement();
    getGameInputElement();
  }

  let getGameInputElement = () => {
    gameInputElement = mswUIInput.gameInput;
  }

  let getGameTitleTextElement = () => {
     pageTitleTextArea = mswUITextArea.titleTextArea;
     pageTitleTextArea.setTextContent(`MSW Tavern`);
  }

  let getMainBodyTextElement = () => {
     pageBodyTextArea = mswUITextArea.bodyTextArea;
     pageBodyTextArea.setTextContent(`
     msw starting point
     people travel here to find their life.
     npc: bartender, waitress, cooker
     player: ion, ozark
     exit: e,w,s,n,u,d
     `)
  }

  let getPageContainers = () => {
   
     pageContainer = mswUIContainer.pageContainer;
     pageTitleContainer = mswUIContainer.pageTitleContainer;
     pageBodyContainer = mswUIContainer.pageBodyContainer;
     pageInputContainer = mswUIContainer.pageInputContainer;
     pageBtnContainer = mswUIContainer.pageBtnContainer;

  }

  let updateGameElementToContainer = () => {
    loginInputElement = mswUIInput.loginInput;
    pageInputContainer.node.removeChild(loginInputElement.node);
    pageInputContainer.node.appendChild( gameInputElement.node);
    yesBtn = mswUIBtn.yesNewCharBtn;
    noBtn = mswUIBtn.noNewCharBtn;
   // pageBtnContainer.node.removeChild(yesBtn.node);
  //  pageBtnContainer.node.removeChild(noBtn.node);
  }

  let updatePageContainer = () => {
   // pageContainer.node.removeChild(pageBtnContainer.node);
   //  pageContainer.node.appendChild( pageTitleContainer.node);
   //  pageContainer.node.appendChild( pageBodyContainer.node);
     pageContainer.node.appendChild( pageInputContainer.node);
  //   pageContainer.node.appendChild( pageBtnContainer.node);
   // document.body.appendChild( pageContainer.node);
 
  }

 
  " 2. render game-Page: ";
  "   a: roomTitle-textArea";
  "   b: roomDescription-textArea";
  "   c: npc-name-status-textArea";
  "   d: room-exit-direction-textArea";
  "   e: cmd-inputArea";
  " 3. add gameCmdListener to cmd-inputarea";

let renderGamePage = ()=>{
  getGamePageUIElements();  
  getPageContainers();
  updateGameElementToContainer();
  updatePageContainer();
//  setDocumentBodyStyle();   
};

    //get roomId by roomIdMap[residentUUID, roomId]
    // if roomId=== undefine, set room at mswbistro, player lands
    // else
    // get roomTitle by roomTitleMap[roomId, roomTitle]
    // get roomdescription by roomDescriptionMap[roomId, description]
    // get npc by npcIdMap[roomId, npcId]
    // get npcname by npcNameMap[npcId, npcname]
    // get roomExit by roomExitMap[roomId, exitDirection]
    // get cmdInputNode, add cmdInputListener

  //game-page:
  //1.room-id-name
  //2.room-short-description
  //3.room-long-description
  //4.room-exit(connection)-direction
  //5.check for any npc
  //6.check other players
  //7.npc action(if any)
  //8.interaction with npc if any


export { renderGamePage };

*/
