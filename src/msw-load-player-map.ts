import { mswDirPath } from "./msw-dir-path";
import * as fs from "fs";

class MSWLoadPlayerMap {
  fsPromises = fs.promises;
  playerTextMapDirPath = mswDirPath.getPlayerTextMapDir();
  playerValueMapDirPath = mswDirPath.getPlayerValueMapDir();

  loadTextMap = async (mapName: string):Promise<string> => {
    let fileType = `json`;
    let filePath = this.playerTextMapDirPath + `/` + mapName + `.` + fileType;
    return await this.fsPromises.readFile(filePath, `utf8`)
                   .catch((e) => {
                            console.log(`e:${e.code}; emessage: ${e.message}`)
                            if (e.code === `ENOENT`){
                                return (`ENOENT`);
                            } else {     
                                throw (e);
                            }
                         });
  }

  loadValueMap = async (mapName: string): Promise<string> => {
    let fileType = `json`;
    let filePath = this.playerValueMapDirPath + `/` + mapName + `.` + fileType; 
    return await this.fsPromises.readFile(filePath, `utf8`)
                    .catch((e) => {
                      console.log(`load player value map e:${e.code}`)
                      if (e.code === `ENOENT`){
                        return (`ENOENT`);
                      } else {
                        throw (e);
                      }
                    });
  }

  writeFile = async (thisFilePath: string, data: string) => {
    await this.fsPromises.writeFile(thisFilePath, data, `utf8`)
        .catch((e) => {
          console.log(`${e.code} in writeFile`);
          throw (e)
        })    
  }

}

let mswLoadPlayerMap = new MSWLoadPlayerMap();
export { mswLoadPlayerMap };
