
import { UIElement } from "./ui-element";
import { roomElementContainers } from "./mswElementContainers/room-element-containers";
import { roomElements } from "./mswElements/room-elements";

let messageText: UIElement;
let roomBodyContainer: UIElement;
let roomBody: UIElement;

let messageLastTime = async (ms:number):Promise<void> => {
  return new Promise((resolve) => {
      setTimeout(()=>{
        roomBodyContainer.node.removeChild(messageText.node);
        roomBodyContainer.node.appendChild(roomBody.node);
        resolve();
      }
      , ms)
      })
  }

let renderWarning = async (message: string):Promise<void> => {
  messageText = new UIElement('text');
  messageText.setTextContent(message);
  messageText.setStyle('message-text');
  roomBody = roomElements.roomBody;
  roomBodyContainer = roomElementContainers.roomBodyContainer;
  roomBodyContainer.node.removeChild(roomBody.node);
  roomBodyContainer.node.appendChild(messageText.node);

  await messageLastTime(2000)
        .catch((e)=> { console.log(`${e.code} in showWarning`)});
  }

export { renderWarning }
