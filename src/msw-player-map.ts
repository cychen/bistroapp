import { mswStorageDataMap } from "./msw-storage-datamap";
import { mswLoadPlayerMap } from "./msw-load-player-map";

class MSWPlayerMap {

  getTextMap = async (mapName: string):Promise<Map<string, string>> => {  
    let storageKey: string = mapName;
    let playerStorageTextMap = mswStorageDataMap.getPlayerStorageTextMap();
    let textMap: Map<string, string> = playerStorageTextMap.get(storageKey);
 
    if (textMap === undefined){
      let loadInData: string = await mswLoadPlayerMap.loadTextMap(mapName)
          .catch((e)=> { throw (e); });   
      if (loadInData === `ENOENT`){
        textMap = new Map();
      } else {
        textMap = new Map(Object.entries(JSON.parse(loadInData)));
      };
      this.updateTextMap(mapName,textMap);
    } 
    return textMap;    
  };

  getValueMap = async (mapName: string):Promise<Map<string, number>> => {
    let storageKey: string = mapName;
    let playerStorageValueMap = mswStorageDataMap.getPlayerStorageValueMap();
    let valueMap: Map<string, number> = playerStorageValueMap.get(storageKey);
    if (valueMap === undefined){
      let loadInData: string = await mswLoadPlayerMap.loadValueMap(mapName)
          .catch((e) => { throw (e);});
      if (loadInData === `ENOENT`){
        valueMap = new Map();
      } else {
        valueMap = new Map(Object.entries(JSON.parse(loadInData)));
      };
      this.updateValueMap(mapName, valueMap);
    }
    return valueMap;
  }

  updateTextMap = (mapName: string, textMap: Map<string, string>) => {
    mswStorageDataMap.updatePlayerStorageTextMap(mapName,textMap);
  }

  updateValueMap = (mapName: string, valueMap: Map<string, number>) => {
    mswStorageDataMap.updatePlayerStorageValueMap(mapName, valueMap);
  }


 
}

let mswPlayerMap = new MSWPlayerMap();
export { mswPlayerMap };