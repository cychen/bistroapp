
//class MSWRegExpCheck {

let regExpCheck = (inputText: string): [boolean, string] => {
  if (inputText.length === 0) {
    return [false, 'no input'];
  } else if (testIllegalCharacter(inputText)) {
    return [false, 'contains illegal character'];
  } else {
    return [true, inputText];
  }
}

let testIllegalCharacter = (inputText: string): boolean => {
  //\w => includes [a-zA-Z0-9_]
  //wordReg => allowed characters: [chinese,-,a-zA-Z0-9_];
  //[^\w] => negated \w selection
  let wordReg = /[^\u4e00-\u9fa5\-\w]+/;
  return wordReg.test(inputText);
}

//}

//let mswRegExpCheck = new MSWRegExpCheck();
export { regExpCheck }
