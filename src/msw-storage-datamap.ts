
import * as fs from "fs";
import { v4 as uuidV4 } from "uuid";
import * as path from "path";

//import { makePlayerDir } from "./msw-game-preparation/make-player-dir";

`
player related data:
1.id
2.name
3.money
4.location -> map<playerUUID, $roomId>
5.equippedGears:
  a: check itemOwnerShip from map<itemId, ownerId>; return listOfItemId
  b: for item in listOfItemId, combine with "whereItIs => playerEquipped" to find playerEquippedGears
  c: for item in "playerEquippedGears", combine with "equipToBodyPart", to show playerEquipGears
6.itemsInPlayerBag:
  a: same as step a in "equippedGears"
  b: combine with "whereItIs => playerBag"
7.itemsInPlayerInventory:
  a: same as step a in "equippedGears"
  b: combine with "whereItIs" => playerInventory"


dataFile: playerData
1: "name" => map<uuid, name>; textMap
2: "money" => map<uuid, gold>; valueMap
3: "location" => map<uuid, roomId>; textMap

dataFile: mswItemData => give Each Item an Id
1: "name" => map<itemId, name> textMap
2: "description" => map<itemId, description> textMap
3: "equippable" => map<itemId, boolean> booleanMap
4: "wearAndTearable" => map<itemId, boolean> booleanMap
5: "stackable" => map<itemId, boolean> booleanMap
6: "equipToBodyPart" = map<itemId, bodypart> textMap
7: "wearAndTearDegree" => map<itemId, wearAndTearPercent> valueMap
8: "maxStackQty" => map<itemId, qty> valueMap
9: "ownership" => map<itemId, ownerId> textMap
10: "whereItIs" => map<itemId, itemLocation> textMap
11: "quantity" => map<itemId, qty> valueMap

room related data:
1: id
2: name
3: description
4: exit: ewsnud
   [e, toRoomIdE];[w, toRoomIdW];..
5: npc, other players, items,..=> recorded in location map
    
dataFile: mswRoomData
1:"name" => map<id, name> textMap
2:"description" => map<id, description> textMap
3:"exit" => map<id, map<dir, neighborRoomId> exitMap

dataMap: textMap, valueMap, booleanMap, mapOfTextMap
storageTextMap: Map<string, Map<string, string>> = new Map();
storageValueMap: Map<string, Map<string, number>> = new Map();
storageBooleanMap: Map<string, Map<string, boolean>> = new Map();
storageExitMap: Map<string, Map<string, string>> = new Map();


example:
 textMap = map<itemuuid, itemstring>
 storageTextMap= Map<storageKey, textMap>
 each textMap is stored as individual file in local disk
 init-func get called by game-start-page to clear storageXXMap
 dataEmitter.on("getTextMap", storagekey)
 call func to get textMap: storageTextMap.get(storagekey)
 a. if value !== undefine
    return value to caller
 b. if value === undefine
      try to load "{storageKey}"TextMap file
        if found; thisTextMap = load "{storageKey}"TextMap
       else: thisTextMap = new Map()
      update (storageKey, thisTextMap) to storageTextMap
      return "{storageKey}"TextMap to caller
`

class MSWStorageDataMap {
  private playerKeyVsTypeMap: Map<string, string>;
  private playerStorageTextMap: Map<string, Map<string, string>>;
  private playerStorageValueMap: Map<string, Map<string, number>>;

  initMap = () => {  
    this.playerStorageTextMap = new Map();
    this.playerStorageValueMap = new Map();
    this.playerKeyVsTypeMap = new Map();
  };

  getPlayerStorageTextMap = (): Map<string, Map<string, string>> => {
    return this.playerStorageTextMap;
  }
  
  getPlayerStorageValueMap = (): Map<string, Map<string, number>> => {
    return this.playerStorageValueMap;
  }

  updatePlayerStorageTextMap = (storageKey: string, textMap:Map<string, string>) => {
    this.playerStorageTextMap.set(storageKey, textMap);
  }

  updatePlayerStorageValueMap = (storageKey: string, valueMap:Map<string, number>) => {
    this.playerStorageValueMap.set(storageKey, valueMap);
  }


}

let mswStorageDataMap = new MSWStorageDataMap();
export { mswStorageDataMap };
