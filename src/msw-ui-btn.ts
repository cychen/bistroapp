import { UIElement } from "./ui-element";

import { styleButton } from "./mswStyle/style-button";


class MSWUIBtn {
  gameStartBtn: UIElement;
  yesNewCharBtn: UIElement;
  noNewCharBtn: UIElement;
  hambergerBtn: UIElement;
  action1Btn: UIElement;
  action2Btn: UIElement;
  action3Btn: UIElement;

  constructor(){
    this.gameStartBtn = this.gameToStartBtn();
    this.yesNewCharBtn = this.yesToNewCharBtn();
    this.noNewCharBtn = this.noToNewCharBtn();
    this.hambergerBtn = this.gameMenuBtn();
    this.action1Btn = this.getAction1Btn();
    this.action2Btn = this.getAction2Btn();
    this.action3Btn = this.getAction3Btn();
  }

  private uiBtn = (): UIElement => {
    let btn = new UIElement('button');
    btn.setAttribute('type', 'button');
    return btn;
  }

  private getAction1Btn = (): UIElement => {
    return this.uiBtn()
               .setTextContent(`Action1`)
               .setStyle(styleButton.actionBtnStyle());
  }

  private getAction2Btn = (): UIElement => {
    return this.uiBtn()
               .setTextContent(`Action1`)
               .setStyle(styleButton.actionBtnStyle());
  }
  private getAction3Btn = (): UIElement => {
    return this.uiBtn()
               .setTextContent(`Action1`)
               .setStyle(styleButton.actionBtnStyle());
  }
  private gameMenuBtn = (): UIElement => {
    return this.uiBtn()
               .setTextContent('menu')
               .setStyle(styleButton.actionBtnStyle());
  }

  private gameToStartBtn = (): UIElement => {
    return this.uiBtn()
               .setTextContent('start')
               .setStyle(styleButton.actionBtnStyle());
  }
 
  private yesToNewCharBtn = (): UIElement => {
    return this.uiBtn()
               .setTextContent(`Yes`)
               .setStyle(styleButton.actionBtnStyle());
  }

  private noToNewCharBtn = (): UIElement => {
    return this.uiBtn()
               .setTextContent(`No`)
               .setStyle(styleButton.actionBtnStyle());
  }

 
  
}

let mswUIBtn = new MSWUIBtn();
export { mswUIBtn }