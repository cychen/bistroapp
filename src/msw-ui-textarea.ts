/*
import { UIElement } from "./ui-element";
import { gameText } from "./msw-game-text";
import { styleTextarea } from "./mswStyle/style-textarea";


class MSWUITextArea {
  titleTextArea: UIElement;
  bodyTextArea: UIElement;
  messageTextArea: UIElement;

  constructor(){
    this.titleTextArea = this.gameTitleTextArea();
    this.bodyTextArea = this.gameBodyTextArea();
    this.messageTextArea = this.gameMessageTextArea();
  }

  private textArea = (): UIElement => {
    let textArea = new UIElement("textarea");
    textArea.setAttribute("readonly", "true");
    textArea.setAttribute("disabled", "true");
    return textArea;
  };

  private gameTitleTextArea = (): UIElement => {
    return this.textArea()
          //  .setStyle(styleTextarea.gameTitleStyle())
            .setTextContent(gameText.gameTitleText());
  };
 
  private gameBodyTextArea = (): UIElement => {
    return this.textArea()
         //   .setStyle(styleTextarea.welcomeStyle())
            .setTextContent(gameText.welcomeText());   
  };

  private gameMessageTextArea = (): UIElement => {
    return this.textArea()
            .setStyle(styleTextarea.messageStyle())
            .setTextContent('');

  }

  newCharacterTextArea = (): UIElement => {
    return this.textArea()
              //  .setStyle(styleTextarea.welcomeStyle())
                .setTextContent(gameText.newCharacterText());
  }


}

let mswUITextArea = new MSWUITextArea();
export { mswUITextArea };

*/
