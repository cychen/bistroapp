
import { mswStorageDataMap } from "./msw-storage-datamap";
import { renderWelcomePage } from "./msw-welcome-page-render";


(()=>{  
  renderWelcomePage();    
  mswStorageDataMap.initMap();    
})()


