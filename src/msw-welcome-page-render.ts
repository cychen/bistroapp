import { UIElement } from "./ui-element";
import { gameText } from "./msw-game-text";
import { pageFrames } from "./mswRenderFrame/page-frames";
import { pageHeadFrames } from "./mswRenderFrame/page-head-frames";
import { pageBodyFrames } from "./mswRenderFrame/page-body-frames";
import { roomFrames } from "./mswRenderFrame/room-frames";
import { actionFrames } from "./mswRenderFrame/action-frames";
import { gameElements } from "./mswElements/game-elements";
import { gameElementContainers } from "./mswElementContainers/game-element-containers";
import { roomElements } from "./mswElements/room-elements";
import { inputElements } from "./mswElements/input-elements";
import { actionElements } from "./mswElements/action-elements";
import { roomElementContainers } from "./mswElementContainers/room-element-containers";
import { inputElementContainers } from "./mswElementContainers/input-element-containers";
import { actionElementContainers } from "./mswElementContainers/action-element-containers";
import { gameAlignContainers } from "./mswElementAlignContainers/game-align-containers";
import { roomAlignContainers } from "./mswElementAlignContainers/room-align-containers";
import { inputAlignContainers } from "./mswElementAlignContainers/input-align-containers";
import { actionAlignContainers } from "./mswElementAlignContainers/action-align-containers";
import { loginLs } from "./mswInputLs/loginLs";

let mainPage: UIElement;
let pageHeadSection: UIElement;
let pageBodySection: UIElement;
let gameTitleSection: UIElement;
let gameTitleAlignContainer: UIElement;
let gameTitleContainer: UIElement;
let gameTitle: UIElement;
let gameSubTitleSection: UIElement;
let gameSubTitleAlignContainer: UIElement;
let gameSubTitleContainer: UIElement;
let gameSubTitle: UIElement;
let gameMenuSection: UIElement;
let gameMenuAlignContainer: UIElement;
let gameMenuContainer: UIElement;
let gameMenu: UIElement;
let gameRoomSection: UIElement;
let roomTitleSection: UIElement;
let roomTitleAlignContainer: UIElement;
let roomTitleContainer: UIElement;
let roomTitle: UIElement;
let roomBodySection: UIElement;
let roomBodyAlignContainer: UIElement;
let roomBodyContainer: UIElement;
let roomBody: UIElement;
let inputSection: UIElement;
let inputAlignContainer: UIElement;
let inputContainer: UIElement;
let loginInput: UIElement;
let gameActionSection: UIElement;
let actMenuSection: UIElement;
let act1AlignContainer: UIElement;
let act1Container: UIElement;
let act2AlignContainer: UIElement;
let act2Container: UIElement;
let act1: UIElement;
let act2: UIElement;

let getGameElements = () => {
  gameTitle = gameElements.gameTitle
    .setTextContent('MSW');
  gameSubTitle = gameElements.gameSubTitle
    .setTextContent('mountain sea world');
  gameMenu = gameElements.gameMenu
    .setTextContent('menu');
}

let getGameElementContainers = () => {
  gameTitleContainer = gameElementContainers.gameTitleContainer;
  gameSubTitleContainer = gameElementContainers.gameSubTitleContainer;
  gameMenuContainer = gameElementContainers.gameMenuContainer;
  gameTitleContainer.node.appendChild(gameTitle.node);
  gameSubTitleContainer.node.appendChild(gameSubTitle.node);
  gameMenuContainer.node.appendChild(gameMenu.node);
}

let getGameAlignContainers = () => {
  gameTitleAlignContainer = gameAlignContainers.gameTitleAlignContainer;
  gameSubTitleAlignContainer = gameAlignContainers.gameSubTitleAlignContainer;
  gameMenuAlignContainer = gameAlignContainers.gameMenuAlignContainer;
  gameTitleAlignContainer.node.appendChild(gameTitleContainer.node);
  gameSubTitleAlignContainer.node.appendChild(gameSubTitleContainer.node);
  gameMenuAlignContainer.node.appendChild(gameMenuContainer.node);
}

let getRoomElements = () => {
  roomTitle = roomElements.roomTitle
    .setTextContent('roomTitle');
  roomBody = roomElements.roomBody
    .setTextContent(`${gameText.welcomeText()}`);
}

let getRoomElementContainers = () => {
  roomTitleContainer = roomElementContainers.roomTitleContainer;
  roomBodyContainer = roomElementContainers.roomBodyContainer;
  roomTitleContainer.node.appendChild(roomTitle.node);
  roomBodyContainer.node.appendChild(roomBody.node);
}

let getRoomAlignContainers = () => {
  roomTitleAlignContainer = roomAlignContainers.roomTitleAlignContainer;
  roomBodyAlignContainer = roomAlignContainers.roomBodyAlignContainer;
  roomTitleAlignContainer.node.appendChild(roomTitleContainer.node);
  roomBodyAlignContainer.node.appendChild(roomBodyContainer.node);
}

let getLoginInputElement = () => {
  loginInput = inputElements.loginInput;
  inputElements.defaultLoginInputValue();
}

let listenToLoginInput = () => {
  loginInput.node.addEventListener('change', loginLs);
}

let getInputElementContainers = () => {
  inputContainer = inputElementContainers.inputContainer;
  inputContainer.node.appendChild(loginInput.node);
}

let getInputAlignContainers = () => {
  inputAlignContainer = inputAlignContainers.inputAlignContainer
  inputAlignContainer.node.appendChild(inputContainer.node);
}

let getActionElements = () => {
  act1 = actionElements.act1
    .setTextContent('ACT1');
  act2 = actionElements.act2
    .setTextContent('ACT2');
}

let getActionElementContainers = () => {
  act1Container = actionElementContainers.act1Container;
  act2Container = actionElementContainers.act2Container;
  act1Container.node.appendChild(act1.node);
  act2Container.node.appendChild(act2.node);
}

let getActionAlignContainers = () => {
  act1AlignContainer = actionAlignContainers.act1AlignContainer;
  act2AlignContainer = actionAlignContainers.act2AlignContainer;
  act1AlignContainer.node.appendChild(act1Container.node);
  act2AlignContainer.node.appendChild(act2Container.node);
}

let wrapAlignmentContainerToSections = () => {
  gameTitleSection.node.appendChild(gameTitleAlignContainer.node);
  gameSubTitleSection.node.appendChild(gameSubTitleAlignContainer.node);
  gameMenuSection.node.appendChild(gameMenuAlignContainer.node);
  roomTitleSection.node.appendChild(roomTitleAlignContainer.node);
  roomBodySection.node.appendChild(roomBodyAlignContainer.node);
  inputSection.node.appendChild(inputAlignContainer.node);
  actMenuSection.node.appendChild(act1AlignContainer.node);
  actMenuSection.node.appendChild(act2AlignContainer.node);
}

let getMainPageFrames = () => {
  mainPage = pageFrames.mainPage;
  pageHeadSection = pageFrames.pageHeadSection;
  pageBodySection = pageFrames.pageBodySection;
  mainPage.node.appendChild(pageHeadSection.node);
  mainPage.node.appendChild(pageBodySection.node);
}
let getPageHeadFrames = () => {
  gameTitleSection = pageHeadFrames.gameTitleSection;
  gameSubTitleSection = pageHeadFrames.gameSubTitleSection;
  gameMenuSection = pageHeadFrames.gameMenuSection;
  pageHeadSection.node.appendChild(gameTitleSection.node);
  pageHeadSection.node.appendChild(gameSubTitleSection.node);
  pageHeadSection.node.appendChild(gameMenuSection.node);
}
let getPageBodyFrames = () => {
  gameRoomSection = pageBodyFrames.gameRoomSection;
  gameActionSection = pageBodyFrames.gameActionSection;
  pageBodySection.node.appendChild(gameRoomSection.node);
  pageBodySection.node.appendChild(gameActionSection.node);
}
let getRoomFrames = () => {
  roomTitleSection = roomFrames.roomTitleSection;
  roomBodySection = roomFrames.roomBodySection;
  gameRoomSection.node.appendChild(roomTitleSection.node);
  gameRoomSection.node.appendChild(roomBodySection.node);
}
let getActionFrames = () => {
  inputSection = actionFrames.inputSection;
  actMenuSection = actionFrames.actMenuSection;
  gameActionSection.node.appendChild(inputSection.node);
  gameActionSection.node.appendChild(actMenuSection.node);
}

let getWelcomePageComponents = () => {
  getMainPageFrames();
  getPageHeadFrames();
  getGameElements();
  getGameElementContainers();
  getGameAlignContainers();
  getPageBodyFrames();
  getRoomFrames();
  getRoomElements();
  getRoomElementContainers();
  getRoomAlignContainers();
  getActionFrames();
  getLoginInputElement();
  getInputElementContainers();
  getInputAlignContainers();
  getActionElements();
  getActionElementContainers();
  getActionAlignContainers();
  wrapAlignmentContainerToSections();
}

let renderWelcomePage = () => {
  getWelcomePageComponents();
  listenToLoginInput();
  document.body.appendChild(mainPage.node);
};

export { renderWelcomePage };
