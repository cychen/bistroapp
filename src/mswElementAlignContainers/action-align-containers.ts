import { UIElement } from "../ui-element";

class ActionAlignContainers {
  act1AlignContainer: UIElement;
  act2AlignContainer: UIElement;

  constructor (){
    this.act1AlignContainer = new UIElement('div')
        .setStyle('content-center-container');
    this.act2AlignContainer = new UIElement('div')
        .setStyle('content-center-container');        
 } 

}

let actionAlignContainers = new ActionAlignContainers();
export { actionAlignContainers } 