import { UIElement } from "../ui-element";

class GameAlignContainers {
  gameTitleAlignContainer: UIElement;
  gameSubTitleAlignContainer: UIElement;
  gameMenuAlignContainer: UIElement;

  constructor (){
    this.gameTitleAlignContainer = new UIElement('div')
        .setStyle('content-left-container');
    this.gameSubTitleAlignContainer = new UIElement('div')
        .setStyle('content-left-container');
    this.gameMenuAlignContainer = new UIElement('div')
        .setStyle('content-center-container');

 } 

}

let gameAlignContainers = new GameAlignContainers();
export { gameAlignContainers } 