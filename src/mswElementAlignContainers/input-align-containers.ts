import { UIElement } from "../ui-element";

class InputAlignContainers {
  inputAlignContainer: UIElement;

  constructor (){
    this.inputAlignContainer = new UIElement('div')
        .setStyle('content-left-container');
     
 } 

}

let inputAlignContainers = new InputAlignContainers();
export { inputAlignContainers } 