import { UIElement } from "../ui-element";

class RoomAlignContainers {
  roomTitleAlignContainer: UIElement;
  roomBodyAlignContainer: UIElement;

  constructor (){
    this.roomTitleAlignContainer = new UIElement('div')
        .setStyle('content-left-container');
    this.roomBodyAlignContainer = new UIElement('div')
        .setStyle('content-center-container');    
 } 

}

let roomAlignContainers = new RoomAlignContainers();
export { roomAlignContainers } 