import { UIElement } from "../ui-element";

class ActionElementContainers {
  act1Container: UIElement;
  act2Container: UIElement;
  
  constructor (){
    this.act1Container = new UIElement('div')
        .setStyle('text-container');
    this.act2Container = new UIElement('div')
        .setStyle('text-container');       
 } 

}

let actionElementContainers = new ActionElementContainers();
export { actionElementContainers } 