import { UIElement } from "../ui-element";

class GameElementContainers {
  gameTitleContainer: UIElement;
  gameSubTitleContainer: UIElement;
  gameMenuContainer: UIElement;
  
  constructor (){
    this.gameTitleContainer = new UIElement('div')
        .setStyle('text-container');
    this.gameSubTitleContainer = new UIElement('div')
        .setStyle('text-container'); 
    this.gameMenuContainer = new UIElement('div')
        .setStyle('text-container');
 
 } 

}

let gameElementContainers = new GameElementContainers();
export { gameElementContainers } 