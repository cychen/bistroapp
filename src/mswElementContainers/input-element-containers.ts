import { UIElement } from "../ui-element";

class InputElementContainers {
  inputContainer: UIElement;

  constructor (){
    this.inputContainer = new UIElement('div')
        .setStyle('input-text-container');
  }

}

let inputElementContainers = new InputElementContainers();
export { inputElementContainers } 