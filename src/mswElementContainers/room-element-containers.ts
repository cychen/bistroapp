import { UIElement } from "../ui-element";

class RoomElementContainers {
  roomTitleContainer: UIElement;
  roomBodyContainer: UIElement;

  constructor (){
    this.roomTitleContainer = new UIElement('div')
        .setStyle('text-container');
    this.roomBodyContainer = new UIElement('div')
        .setStyle('text-container');     
 } 

}

let roomElementContainers = new RoomElementContainers();
export { roomElementContainers } 