import { UIElement } from "../ui-element";

class ActionElements {
  act1: UIElement;
  act2: UIElement;

  constructor (){
    this.act1 = new UIElement('text')
        .setStyle('title-text');
    this.act2 = new UIElement('text')
        .setStyle('title-text'); 

 } 

}

let actionElements = new ActionElements();
export { actionElements} 