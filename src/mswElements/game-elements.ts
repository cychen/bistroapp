import { UIElement } from "../ui-element";

class GameElements {
  gameTitle: UIElement;
  gameSubTitle: UIElement;
  gameMenu: UIElement;

  constructor (){
    this.gameTitle = new UIElement('text')
        .setStyle('title-text');
    this.gameSubTitle = new UIElement('text')
        .setStyle('sub-title-text'); 
    this.gameMenu = new UIElement('text')
        .setStyle('menu');

 } 

}

let gameElements = new GameElements();
export { gameElements } 