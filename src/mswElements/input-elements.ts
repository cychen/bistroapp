import { UIElement } from "../ui-element";

class InputElements {
  loginInput: UIElement;
  newCharInput: UIElement;
  loginInputNode: HTMLInputElement;
  newCharInputNode: HTMLInputElement;

  constructor() {
    this.loginInput = this.input();
    this.newCharInput = this.input();
    this.loginInputNode = this.loginInput.node as HTMLInputElement;
    this.newCharInputNode = this.newCharInput.node as HTMLInputElement;
  }

  input = (): UIElement => {
    return new UIElement('input')
      .setAttribute('type', 'text')
      .setAttribute('required', 'true')
      .setAttribute('placeholder', 'cmd')
      .setStyle('input-text');
  }

  getLoginInputValue = (): string => {
    return this.loginInputNode.value
  }

  getNewCharInputValue = (): string => {
    return this.newCharInputNode.value
  }

  defaultLoginInputValue = () => {
    this.loginInputNode.value = '';
    this.loginInput.setAttribute('placeholder', 'enter charName');
  }

  defaultNewCharInputValue = () => {
    this.newCharInputNode.value = '';
    this.newCharInput.setAttribute('placeholder', 'enter y/n new char');
  }

  disableLoginInput = () => {
    this.loginInput.setAttribute('disabled', 'true');
  }

  disableNewCharInput = () => {
    this.newCharInput.setAttribute('disabled', 'true');
  }

  enableLoginInput = () => {
    this.loginInput.removeAttribute('disabled');
  }

  enableNewCharInput = () => {
    this.newCharInput.removeAttribute('disabled');
  }

}

let inputElements = new InputElements();
export { inputElements }
