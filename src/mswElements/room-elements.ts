import { UIElement } from "../ui-element";

class RoomElements {
  roomTitle: UIElement;
  roomBody: UIElement;

  constructor (){
    this.roomTitle = new UIElement('text')
        .setStyle('title-text');
    this.roomBody = new UIElement('text')
        .setStyle('body-text'); 

 } 

}

let roomElements = new RoomElements();
export { roomElements } 