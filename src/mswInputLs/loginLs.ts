
import { regExpCheck } from "../msw-regexp-check";
import { checkLogin } from "../msw-check-login";
import { renderWarning } from "../msw-message-render";
import { inputElements } from "../mswElements/input-elements";

let loginLs = async () => {
  console.log('into login ls')
  inputElements.disableLoginInput();
  let loginText = inputElements.getLoginInputValue();
  let [inputFormatOk, inputText]: [boolean, string] = regExpCheck(loginText);
  if (inputFormatOk) {
    console.log(`check name: ${inputText}`);
    await checkLogin();
  } else {
    await renderWarning(inputText);
    inputElements.defaultLoginInputValue();
    inputElements.enableLoginInput();
    console.log(`warning:${inputText}`);
  }
}


export { loginLs }
