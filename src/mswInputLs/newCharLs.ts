import { regExpCheck } from "../msw-regexp-check";
import { renderWarning } from "../msw-message-render";
import { inputElements } from "../mswElements/input-elements";

let createNewChar = () => {

}
let confirmationForNewChar = () => {
  let yesReg = /[Yy]/;
  let noReg = /[Nn]/;
  let newCharInput = inputElements.getNewCharInputValue();
  if (yesReg.test(newCharInput)) {
    console.log('yes to create new char')
    //go to create new char with charname:
    createNewChar();
  } else if (noReg.test(newCharInput)) {
    console.log('no to create new char')
    // message not to create new char with charname:
    //back to welcome page to accept charname input;
  } else {
    console.log('cmd not recognized');
    //message: cmd not recognized
    //reset newCharInput value
    inputElements.defaultNewCharInputValue();
  }
}

let newCharLs = async () => {
  console.log('into new char ls')
  inputElements.disableNewCharInput();
  let newCharInputValue = inputElements.getNewCharInputValue();
  let [inputFormatOk, inputText]: [boolean, string] = regExpCheck(newCharInputValue);
  if (inputFormatOk) {
    console.log(`check answer: ${inputText}`);
    confirmationForNewChar();
  } else {
    await renderWarning(inputText);
    inputElements.defaultNewCharInputValue();
    console.log(`warning:${inputText}`);
  }
}


export { newCharLs }
