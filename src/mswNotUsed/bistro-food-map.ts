//'use strict';
/*
import * as fs from 'fs';
import { IpcMainEvent, ipcMain, EventEmitter } from 'electron';
import { findFilePath } from './file-path';

class BistroFoodMap {

  foodMap: Map<string, string>;
  private filePath = findFilePath('food');

  constructor() {
    this.foodMap = new Map();
  }

  initFoodMap = (initEmitter: EventEmitter) => {
    fs.access(this.filePath, (err) => {
      if (!err) {
        this.loadFile(initEmitter);
      } else {
        this.generatingFoodMap(initEmitter);
      }
    });
  }


  private loadFile = (initEmitter: EventEmitter) => {
    fs.readFile(this.filePath, 'utf8', (err, data) => {
      if (!err) {
        this.foodMap = new Map(Object.entries(JSON.parse(data)));
        initEmitter.emit('initOk');
      } else {
        initEmitter.emit(`initError: ${err.message}`);
      }
    });
  }


  private generatingFoodMap = (initEmitter: EventEmitter) => {

    this.foodMap.set('fa', 'BakeYam');
    this.foodMap.set('fb', 'BakePotato');
    this.foodMap.set('fc', 'BakeCorn');
    fs.writeFile(this.filePath, JSON.stringify(Object.fromEntries(this.foodMap)), (err) => {
      if (!err) {
        initEmitter.emit('initOk');
      } else {
        initEmitter.emit('initError', err);
      }
    });
  }

}

let bistroFoodMap = new BistroFoodMap();
export { bistroFoodMap }
*/
