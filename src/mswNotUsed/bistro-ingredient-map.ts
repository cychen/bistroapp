//'use strict';
/*
import * as fs from 'fs';
import { findFilePath } from './file-path';
import { EventEmitter } from 'events';
import * as uuidV4 from 'uuid/v4';


class BistroIngredientMap {

  ingredientMap: Map<string, string>;
  private filePath = findFilePath('ingredient');

  constructor() {
    this.ingredientMap = new Map();
  }

  initIngredientMap = (initEmitter: EventEmitter) => {
    fs.access(this.filePath, (err) => {
      if (!err) {
        this.loadFile(initEmitter);
      } else {
        this.generatingIngredientMap(initEmitter);
      }
    });
  }

  private loadFile = (initEmitter: EventEmitter) => {
    fs.readFile(this.filePath, 'utf8', (err, data) => {
      if (!err) {
        this.ingredientMap = new Map(Object.entries(JSON.parse(data)));
        initEmitter.emit('initOk');
      } else {
        initEmitter.emit('initError', err);
      }
    });
  }


  private generatingIngredientMap = (initEmitter: EventEmitter) => {
    this.ingredientMap.set('ia', 'yam');
    this.ingredientMap.set('ib', 'potato');
    this.ingredientMap.set('ic', 'corn');
    fs.writeFile(this.filePath, JSON.stringify(Object.fromEntries(this.ingredientMap)), (err) => {
      if (!err) {
        initEmitter.emit('initOk');
      } else {
        initEmitter.emit('initError', err);
      }
    });
  }

}

let bistroIngredientMap = new BistroIngredientMap();

export { bistroIngredientMap }
*/
