import { v4 as uuidV4 } from "uuid";

class CmdMap {

  cmdEntityMap: Map<string, string> = new Map();
  characterEntityMap: Map<string, string> = new Map();

  cmdMap: Map<string, (...cmdP: string[]) => void> = new Map();
  constructor() {
    this.cmdMap.set(`loggin`, this.testf);
  }

  cmdId = (cmdName: string): string => {
    let cmdId = this.cmdEntityMap.get(cmdName);
    if (cmdId === undefined) {
      cmdId = uuidV4();
      this.cmdEntityMap.set(cmdName, cmdId);
    };
    return cmdId;
  }

  characterId = (characterName: string): string => {
    let characterId = this.characterEntityMap.get(characterName);
    if (characterId === undefined) {
      characterId = uuidV4();
      this.characterEntityMap.set(characterName, characterId);
    };
    return characterId;
  }


  cmdM = (cmdValue: string) => {
    let cmdF = this.cmdMap.get(cmdValue);
    if (cmdF === undefined) {
      console.log(`cmdF not found`);
    } else {
      cmdF(cmdValue)
    }
  }

  private createCharacter = (characterName: string) => {

  }
  private testf = (cmdValue: string) => {
    console.log(`cmdmap loggin`);
  }
}




let cmdMap = new CmdMap()
export { cmdMap }
