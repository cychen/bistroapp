import { ipcRenderer } from "electron";
import { EventEmitter } from 'events';

//import { createCharacter } from './create-character';
import { cmdMap } from './cmd-map';

class Cmd {

  cmdEmitter: EventEmitter;

  constructor() {
    this.cmdEmitter = new EventEmitter();
  }

  cmdInput = (cmdValue: string) => {
    let cmdArray = cmdValue.split(` `);
    console.log(`cmdArray: ${cmdArray}`);
    cmdMap.cmdM(cmdValue);
  }


}

let cmd = new Cmd();
export { cmd }
