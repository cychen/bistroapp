//import { generateIngredientIdToNameMap } from './ingredientIdToName';
/*
import * as fs from 'fs';
import { findFilePath } from './file-path';
import { ipcMain, IpcMainEvent } from 'electron';
import { EventEmitter } from 'events';
import * as path from 'path';
import { stringify } from 'querystring';
import { UIElement } from './ui-element';
import { defaultPrice } from './default-price';
import * as uuidV4 from 'uuid/v4';

class DataMap {
  dataEmitter = new EventEmitter();
  unlinkEmitter = new EventEmitter();
  textMap: Map<string, Map<string, string>> = new Map();
  valueMap: Map<string, Map<string, number>> = new Map();
  recipeMap: Map<string, Map<string, Map<string, number>>> = new Map();

  //ingredient, using level; no more:primary, secondary
  //condiment, using level, no more basic, special, advance
  //food, using level, no more bake, grill, stirfry
  // recipe 'level' sets number of material needed for cooking

  //any item has basic information: id, name, belongCategory, usage, price..etc.
  //above information can be characterized as: text(string), value(number).

  //textMap includes: categoryMap, nameMap, usageMap, idLinkMap, sellerMap
  //['category', categoryMap],['name', nameMap], ['usage', usageMap], ['idLink', idLinkMap], ['seller', sellerMap]

  //categoryMap includes idToCategoryFoodMap, idToCategoryIngredientMap, idToCategoryCondimentMap, idToCategoryRecipeMap
  //[itemid,'food'],[itemid,'ingredient'],[itemid,'condiment'],[itemid,'recipe']

  //sellerMap includes idToGrocerMeatMap, idToGrocerFreshMap, idToGrocerStarchMap..

  //textMapKey:{'category', 'name', 'usage', 'idLink', 'seller'}
  //textMapValue['category']= Map<itemid, ('ingredient','condiment', 'food', 'recipe')>
  //textMapValue['name'] = Map<itemid, 'potato'..>
  //textMapValue['usage'] = Map<itemid, 'cooking'..'eat'>
  //textMapValue['idLink'] = Map<foodid, recipeid> (link foodid to recipeid for attrs)
  //textMapValue['seller'] = Map<itemid, 'meat'..'fresh'..'starch'..>

  //textMap(key, itemId): <key, map<id,stringvalue>>{
  // key('category')-value: ingredient,condiment,food,recipe;

  //valueMapKey:{'str','foc','agi','lifeRecovery','effectLasting',
  // 'strG','focG','agiG','lifeRecoveryG','effectLastingG',
  // 'price', 'level', 'player'}

  //valueMap(key, itemId):<key, map<id, value>> {
  // valueMap<player,<playerBagItemid, value>>; playerBagItem:ingredient, condiment, food, value:qty

  //recipeMap: <key, <recipeId, map<itemId, value>>>
  // recipeMapKey: 'ingredient', 'condiment'
  constructor() {
    this.dataEmitter.on('getTextMap', (key: string, requesterEvt: EventEmitter) => {
      this.findTextMap(key, requesterEvt);
    });
    this.dataEmitter.on('getValueMap', (key: string, requesterEvt: EventEmitter) => {
      this.findValueMap(key, requesterEvt);
    });
  }

  initDataMap = () => {
    this.textMap.clear();
    this.valueMap.clear();
    this.recipeMap.clear();
    this.testCreateInitPlayerFoodMap();
  }

  testCreateInitPlayerFoodMap = () => {

    let categoryMap = new Map<string, string>();
    categoryMap.set('itemId1', 'food');
    categoryMap.set('itemId2', 'ingredient');
    categoryMap.set('itemId3', 'ingredient');
    categoryMap.set('itemId4', 'ingredient');
    categoryMap.set('itemId5', 'food');
    categoryMap.set('itemId6', 'food');
    categoryMap.set('itemId7', 'food');

    let nameMap = new Map<string, string>();
    nameMap.set('itemId1', 'bakePork');
    nameMap.set('itemId2', 'pork');
    nameMap.set('itemId3', 'onion');
    nameMap.set('itemId4', 'rice');
    nameMap.set('itemId5', 'grillPork');
    nameMap.set('itemId6', 'FRChicken');
    nameMap.set('itemId7', 'FRBeef');

    let sellerMap = new Map<string, string>();
    sellerMap.set('itemId1', 'morc');
    sellerMap.set('itemId2', 'meat');
    sellerMap.set('itemId3', 'fresh');
    sellerMap.set('itemId4', 'starch');
    sellerMap.set('itemId5', 'morc');
    sellerMap.set('itemId6', 'morc');
    sellerMap.set('itemId7', 'morc');

    console.log(`categoryMap: ${JSON.stringify(Object.fromEntries(categoryMap))}`);

    this.textMap.set('category', categoryMap);
    this.textMap.set('seller', sellerMap);
    this.textMap.set('name', nameMap);

    let priceMap = new Map<string, number>();
    priceMap.set('itemId1', 11);
    priceMap.set('itemId2', 22);
    priceMap.set('itemId3', 33);
    priceMap.set('itemId4', 44);
    priceMap.set('itemId5', 20);
    priceMap.set('itemId6', 30);
    priceMap.set('itemId7', 40);

    let playerValueMap = new Map<string, number>();
    playerValueMap.set('itemId1', 1010);
    playerValueMap.set('itemId2', 20);
    playerValueMap.set('itemId5', 50);
    playerValueMap.set('itemId6', 60);
    playerValueMap.set('itemId7', 170);

    this.valueMap.set('player', playerValueMap);
    this.valueMap.set('price', priceMap);

    console.log(`textMap: ${JSON.stringify(Object.fromEntries(this.textMap))}`);
  }

  updateNameOrClassMap = (materialCategory: string, NameOrClass: string, updateMap: Map<string, string>) => {
    let key = `${materialCategory}${NameOrClass}`;
    this.textMap[key] = updateMap;
  }

  updateValueMap = (whatValue: string, updateMap: Map<string, number>) => {
    let key = `${whatValue}`;
    this.valueMap[key] = updateMap;
  }

  getTextMap = (key: string, requesterEvt: EventEmitter) => {
    this.dataEmitter.emit('getTextMap', key, requesterEvt);
  }

  private findTextMap = (key: string, requesterEvt: EventEmitter) => {
    let thisTextMap: Map<string, string> = this.textMap.get(key);
    if (thisTextMap === undefined) {
      requesterEvt.emit(`getTextMap-${key}-ready`, JSON.stringify(Object.fromEntries(new Map<string, string>())));
    } else {
      requesterEvt.emit(`getTextMap-${key}-ready`, JSON.stringify(Object.fromEntries(thisTextMap)));
    };
  }

  getValueMap = (key: string, requesterEvt: EventEmitter) => {
    this.dataEmitter.emit('getValueMap', key, requesterEvt);
  }

  private findValueMap = (key: string, requesterEvt: EventEmitter) => {
    let thisValueMap: Map<string, number> = this.valueMap.get(key);
    if (thisValueMap === undefined) {
      requesterEvt.emit(`getValueMap-${key}-ready`, JSON.stringify(Object.fromEntries(new Map<string, number>())));
    } else {
      requesterEvt.emit(`getValueMap-${key}-ready`, JSON.stringify(Object.fromEntries(thisValueMap)));
    };
  }


}


let dataMap = new DataMap();
export { dataMap }

*/
