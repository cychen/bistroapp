/*
import { IpcMainEvent } from 'electron';
import { EventEmitter } from 'events';
import { dataMap } from './data-map';


class GrocerIngredientItem {

  dataEmitter = new EventEmitter();
  idToSellerMap: Map<string, string> = new Map(); //[itemId, sold by all sellers]
  idArrayByThisSeller: Array<string> = []; //[itemId..] items sold by this seller
  idToNameMap: Map<string, string> = new Map(); //[itemId, name] items sold by any seller
  idToPriceMap: Map<string, number> = new Map(); //[itemId, price] item price
  nameToPriceMap: Map<string, number> = new Map<string, number>(); //[name, price] only items sold by this seller
  category = '';
  seller = '';
  ipcEvt: IpcMainEvent;

  //important: callback func in mapObj.forEach((value, key)=>{}) not (key, value) !!!
  //meatGrocer(蛋肉): egg, pork, beef, chicken
  //freshGrocer(青蔬):'青蔥','青椒','洋蔥','紅蘿蔔'
  //starchGrocer(米糧):'米','麵粉','馬鈴薯','地瓜'

  constructor() {
    this.dataEmitter.on('getGrocerIngredientItem', () => {
      dataMap.getTextMap('seller', this.dataEmitter);
    });
    this.dataEmitter.on('getTextMap-seller-ready', (data: string) => {
      this.reduceSellerMapForThisSeller(data);
    })
    this.dataEmitter.on('reduceSellerMap-ready', () => {
      dataMap.getValueMap('price', this.dataEmitter);
    });
    this.dataEmitter.on('getValueMap-price-ready', (data: string) => {
      this.idToPriceMap = new Map(Object.entries(JSON.parse(data)));
      dataMap.getTextMap('name', this.dataEmitter);
    });
    this.dataEmitter.on('getTextMap-name-ready', (data: string) => {
      this.idToNameMap = new Map(Object.entries(JSON.parse(data)));
      this.nameToPriceMapByThisSeller();
    });
    this.dataEmitter.on('nameToPriceMap-ready', () => {
      this.ipcEvt.reply(`grocerIngredient-${this.seller}-ready`, JSON.stringify(Object.fromEntries(this.nameToPriceMap)));
    });
  }

  //ingredientSeller:'meat','fresh','starch'
  getGrocerIngredientItemMap = (seller: string, evt: IpcMainEvent) => {
    this.category = 'ingredient';
    this.seller = seller;
    this.ipcEvt = evt;
    this.clearMaps();
    this.dataEmitter.emit('getGrocerIngredientItem');
  }

  private clearMaps = () => {
    this.idToSellerMap.clear();
    this.idArrayByThisSeller = [];
    this.idToNameMap.clear();
    this.idToPriceMap.clear();
    this.nameToPriceMap.clear();
  }

  private reduceSellerMapForThisSeller = (data: string) => {
    this.idToSellerMap = new Map(Object.entries(JSON.parse(data)));
    this.idToSellerMap.forEach((seller, itemId) => {
      if (seller === `${this.seller}`) {
        this.idArrayByThisSeller.push(itemId);
      };
    });
    this.dataEmitter.emit('reduceSellerMap-ready');
  }


  private nameToPriceMapByThisSeller = () => {
    this.idArrayByThisSeller.forEach((itemId) => {
      let name = this.idToNameMap.get(itemId);
      let price = this.idToPriceMap.get(itemId);
      if (name === undefined || price === undefined) {
        console.log(`name/price not found`)
      } else {
        this.nameToPriceMap.set(name, price);
      };
    });
    this.dataEmitter.emit('nameToPriceMap-ready');
  }

}

let grocerIngredientItem = new GrocerIngredientItem();
export { grocerIngredientItem }
*/
