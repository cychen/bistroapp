/*
import { UIElement } from './ui-element';
import { ipcRenderer, IpcRendererEvent } from 'electron';
import { modelContainer } from './model-container';
import { modelTable } from './model-table';



'use strict';
class GrocerIngredientRender {
  private meatBtn = new UIElement('button');
  private freshBtn = new UIElement('button');
  private starchBtn = new UIElement('button');
  private grocerIngredientCloseBtn = new UIElement('button');
  private bistroUIBtnArray: Array<UIElement> = [];
  private grocerIngredientBtnArray: Array<UIElement> = [];

  private grocerAllContainer = modelContainer.rowContainer();

  //meatGrocer(蛋肉): egg, pork, beef, chicken
  //freshGrocer(青蔬):'青蔥','青椒','洋蔥','紅蘿蔔'
  //starchGrocer(米糧):'米','麵粉','馬鈴薯','地瓜'

  constructor() {
    this.setUpGrocerIngredientBtnNameAndAttribute();
    this.assembleGrocerIngredientBtnArray();
    this.grocerIngredientCloseBtn.node.addEventListener('click', () => {
      this.grocerIngredientBtnArray.forEach(it => it.removeAttribute('disabled'));
      document.body.removeChild(this.grocerAllContainer.node);
      this.bistroUIBtnArray.forEach(it => it.removeAttribute('disabled'));
    });
    this.meatBtn.node.addEventListener('click', () => {
      this.grocerIngredientBtnArray.forEach(it => it.setAttribute('disabled', 'true'));
      ipcRenderer.send('grocerIngredient', 'meat');
    });
    this.freshBtn.node.addEventListener('click', () => {
      this.grocerIngredientBtnArray.forEach(it => it.setAttribute('disabled', 'true'));
      // ipcRenderer.send('playerCondiment');
    });
    this.starchBtn.node.addEventListener('click', () => {
      this.grocerIngredientBtnArray.forEach(it => it.setAttribute('disabled', 'true'));
      // ipcRenderer.send('playerCondiment');
    });
    ipcRenderer.on('grocerIngredient-meat-ready', (evt: IpcRendererEvent, data: string) => {
      console.log('grocerIngredient-meat-ready');
      this.showGrocerItemMap('Meat', data);
    });
    ipcRenderer.on('grocerIngredient-fresh-ready', (evt: IpcRendererEvent, data: string) => {
      //showPlayerItemMap('Ingredient', data);
    });
    ipcRenderer.on('grocerIngredient-starch-eady', (evt: IpcRendererEvent, data: string) => {
      //showPlayerItemMap('Condiment', data);
    });
  }


  private setUpGrocerIngredientBtnNameAndAttribute = () => {
    this.meatBtn.setTextContent('Meat').setAttribute('type', 'button');
    this.freshBtn.setTextContent(`Fresh`).setAttribute('type', 'button');
    this.starchBtn.setTextContent('Starch').setAttribute('type', 'button');
    this.grocerIngredientCloseBtn.setTextContent('close').setAttribute('type', 'button');
  }

  private assembleGrocerIngredientBtnArray = () => {
    this.grocerIngredientBtnArray.push(this.meatBtn);
    this.grocerIngredientBtnArray.push(this.freshBtn);
    this.grocerIngredientBtnArray.push(this.starchBtn);
    this.grocerIngredientBtnArray.push(this.grocerIngredientCloseBtn);
  }

  listIngredientGrocers = (bistroUIBtnArray: Array<UIElement>) => {
    this.bistroUIBtnArray = bistroUIBtnArray
    document.body.appendChild(this.grocerAllContainer.node);
    this.grocerAllContainer.node.appendChild(this.meatBtn.node);
    this.grocerAllContainer.node.appendChild(this.freshBtn.node);
    this.grocerAllContainer.node.appendChild(this.starchBtn.node);
    this.grocerAllContainer.node.appendChild(this.grocerIngredientCloseBtn.node);
  }

  private showGrocerItemMap = (seller: string, data: string) => {
    let itemMap: Map<string, number> = new Map(Object.entries(JSON.parse(data)));
    let itemTable = modelTable.newTable(`${seller}`);
    let tableBody: UIElement = new UIElement('div');

    if (itemMap.size === 0) {
      let infoLabelArray: Array<UIElement> = [];
      let info = new UIElement('label');
      info.setTextContent(`empty of ${seller}`);
      infoLabelArray.push(info);
      let infoCol = modelTable.addContentToColumn('', infoLabelArray);
      tableBody = modelTable.assembleColumnToTableBody(infoCol);
    } else {
      let nameLabelArray: Array<UIElement> = [];
      let priceLabelArray: Array<UIElement> = [];
      let qtyLabelArray: Array<UIElement> = [];

      for (let entry of itemMap.entries()) {
        let nameLabel = new UIElement('label');
        nameLabel.setTextContent(`${entry[0]}`);
        nameLabelArray.push(nameLabel);
        let priceLabel = new UIElement('label');
        priceLabel.setTextContent(`${entry[1]}`);
        priceLabelArray.push(priceLabel);
        let qtyLabel = new UIElement('label');
        qtyLabel.setTextContent(`10`);
        qtyLabelArray.push(qtyLabel);
      }
      let nameCol = modelTable.addContentToColumn('Name', nameLabelArray);
      let qtyCol = modelTable.addContentToColumn('Qty', qtyLabelArray);
      let priceCol = modelTable.addContentToColumn('Price', priceLabelArray);
      tableBody = modelTable.assembleColumnToTableBody(nameCol, qtyCol, priceCol);
    };
    let closeBtn = new UIElement('button');
    closeBtn.setTextContent('close')
      .setAttribute('type', 'button')

    modelTable.assembleTable(itemTable, tableBody, closeBtn);
    document.body.appendChild(itemTable.node);
    //  .appendToParent(itemTable.node);
    closeBtn.node.addEventListener('click', () => {
      document.body.removeChild(itemTable.node);
      this.grocerIngredientBtnArray.forEach(it => it.removeAttribute('disabled'));
    });
  }

}

let grocerIngredientRender = new GrocerIngredientRender();
export { grocerIngredientRender }

*/
