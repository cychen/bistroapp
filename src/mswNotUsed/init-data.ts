/*
//import { generateIngredientIdToNameMap } from './ingredientIdToName';
import * as fs from 'fs';
import { findFilePath } from './file-path';
import { ipcMain, IpcMainEvent } from 'electron';
import { EventEmitter } from 'events';
import * as path from 'path';
import { dataMap } from './data-map';


class InitData {

  unlinkEmitter = new EventEmitter();

  constructor() {
    this.unlinkEmitter.on('readDir-error', (err: Error) => {
      this.readDirError(err);
    });
    this.unlinkEmitter.on('eraseOldData-start', (filepath, arrayFileNames, initEmitter) => {
      this.unlinkOldData(filepath, arrayFileNames, initEmitter);
    });
    this.unlinkEmitter.on('unlink-error', (err: Error, fileNamePath: string) => {
      this.unlinkError(err, fileNamePath);
    });
    this.unlinkEmitter.on('unlink-ok', (fileNamePath: string) => {
      this.unlinkOk(fileNamePath);
    });
  }


  deleteOldMaps = (initEmitter: EventEmitter) => {
    dataMap.initDataMap();
    let filepath = path.join(__dirname, '../newdata/');
    fs.readdir(filepath, (err, arrayFileNames) => {
      if (err) {
        this.unlinkEmitter.emit('readDir-error', err);
      } else {
        this.unlinkEmitter.emit('eraseOldData-start', filepath, arrayFileNames, initEmitter);
      };
    });
  }

  private readDirError = (err: Error) => {
    console.log(`error on getting dir files: ${err.message}`);
  }

  private unlinkError = (err: Error, fileNamePath: string) => {
    console.log(`${fileNamePath} can not be unlinked`);
  }

  private unlinkOk = (fileNamePath: string) => {
    console.log(`${fileNamePath} is unlinked`);
  }

  private unlinkOldData = (filePath: string, arrayFileNames: Array<string>, initEmitter: EventEmitter) => {
    arrayFileNames.forEach((fileName) => {
      let fileNamePath = `${filePath}${fileName}`;
      fs.unlink(fileNamePath, (err) => {
        if (err) {
          this.unlinkEmitter.emit('unlink-error', err, fileNamePath);
        } else {
          this.unlinkEmitter.emit('unlink-ok', fileNamePath);
        };
      });
    });
    initEmitter.emit('deleteOldData-ok');
  }


}


let initData = new InitData();
export { initData }

*/
