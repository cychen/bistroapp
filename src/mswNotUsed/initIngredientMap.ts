/*
import * as uuidV4 from 'uuid/v4';
import { dataMap } from './data-map';
import { EventEmitter } from 'events';
import { UIElement } from './ui-element';
import { messageStyle } from './style-label';
import * as path from 'path';
import * as fs from 'fs';



class InitIngredientMap {
  readonly primaryIngredientArray: Array<string> = ['麵粉', '米', '雞蛋', '馬鈴薯', '地瓜', '玉米', '豬肉', '牛肉', '雞肉', '魚肉'];
  readonly secondaryIngredientArray: Array<string> = ['洋蔥', '青椒', '紅蘿蔔', '青蔥'];
  readonly initIngredientEmitter = new EventEmitter();
//egg:'雞蛋'
//meat:'豬肉','牛肉','雞肉'
//fresh:'青蔥','青椒','蘑菇','玉米筍','紅蘿蔔'
//dry:'米','麵粉','馬鈴薯','地瓜','洋蔥',

  constructor() {
    this.initIngredientEmitter.on('initNameAndClass-ok', () => {
      this.generateIngredientValueMap();
    })
  }

  updateNameOrClassMap = (category: string, NameOrClass: string, updateMap: Map<string, string>) => {
    dataMap.updateNameOrClassMap(category, NameOrClass, updateMap);
  }


  initIngredientMap = (initEmitter: EventEmitter) => {
    let fileDirPath = path.join(__dirname, '../newdata/');
    let materialCategory = 'ingredient';
    let NameOrClass = 'Name';
    let key = `${materialCategory}${NameOrClass}`;
    let fileName = `${key}Map.json`;
    let fileNamePath = `${fileDirPath}${fileName}`;
    this.unlinkOldData(fileNamePath, this.initIngredientEmitter);
  }


  private generateIngredientNameAndClassMap = () => {
    let ingredientNameMap = new Map();
    let ingredientClassMap = new Map();
    this.pIngredientIdArray = [];
    this.sIngredientIdArray = [];

    for (let pi of this.primaryIngredientArray) {
      let id = uuidV4();
      this.pIngredientIdArray.push(id);
      ingredientNameMap.set(id, pi);
      ingredientClassMap.set(id, 'primary');
    };

    for (let si of this.secondaryIngredientArray) {
      let id = uuidV4();
      this.sIngredientIdArray.push(id);
      ingredientNameMap.set(id, si);
      ingredientClassMap.set(id, 'secondary');
    };

    this.updateNameOrClassMap('ingredient', 'Name', ingredientNameMap);
    this.updateNameOrClassMap('ingredient', 'Class', ingredientClassMap);
    this.initIngredientEmitter.emit('initNameAndClass-ok');
  }


  private generateIngredientValueMap = () => {

  }
  /*
    ingredientIdToCategoryMap.set(id, 'primary');
  strMaterialIdToValueMap.set(id, rollPrimaryMainAttrValue());
  agiMaterialIdToValueMap.set(id, rollPrimaryMainAttrValue());
  focMaterialIdToValueMap.set(id, rollPrimaryMainAttrValue());
  lifeRecoveryMaterialIdToValueMap.set(id, rollPrimarySpecialAttrValue());
  effectLastingMaterialIdToValueMap.set(id, rollPrimarySpecialAttrValue());
  };

  for(let si of secondaryIngredientArray) {
    let id = uuidV4();
    ingredientIdToNameMap.set(id, si);
    ingredientIdToCategoryMap.set(id, 'secondary');
    strMaterialIdToValueMap.set(id, rollSecondaryMainAttrValue());
    agiMaterialIdToValueMap.set(id, rollSecondaryMainAttrValue());
    focMaterialIdToValueMap.set(id, rollSecondaryMainAttrValue());
    lifeRecoveryMaterialIdToValueMap.set(id, rollSecondarySpecialAttrValue());
    effectLastingMaterialIdToValueMap.set(id, rollSecondarySpecialAttrValue());
  };
  }
  }


  let rollPrimaryMainAttrValue = (): number => {
  let attrMax = 10;
  let attrMin = 0;
  return Math.floor(Math.random() * (attrMax - attrMin + 1));
  }

  let rollSecondaryMainAttrValue = (): number => {
  let attrMax = 5;
  let attrMin = 0;
  return Math.floor(Math.random() * (attrMax - attrMin + 1));
  }

  let rollPrimarySpecialAttrValue = (): number => {
  let attrMax = 5;//1unit=10min; 1unit = 1% of baselife per second
  let attrMin = 0;
  return Math.floor(Math.random() * (attrMax - attrMin + 1));
  }

  let rollSecondarySpecialAttrValue = (): number => {
  let attrMax = 3;//1unit=10min; 1unit = 1% of baselife per second
  let attrMin = 0;
  return Math.floor(Math.random() * (attrMax - attrMin + 1));
  }

}

let initIngredientMap = new InitIngredientMap();
export { initIngredientMap }
*/
