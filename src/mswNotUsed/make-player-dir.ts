/*
import * as fs from "fs";
import { mswDirPath } from "./msw-dir-path";

class MakePlayerDir {
  fsPromises = fs.promises;
  playerDirPath = mswDirPath.getPlayerDir();
  textMapDirPath = mswDirPath.getPlayerTextMapDir();
  valueMapDirPath = mswDirPath.getPlayerValueMapDirPath();
  keyVsTypeMapDirPath = mswDirPath.getPlayerKeyVsTypeMapDirPath();

  makeDirFamily = async ()=> {
    await this.makeDir(this.playerDirPath);
    await Promise.all([
      (async ():Promise<void> => { await this.makeDir(this.textMapDirPath)})(),
      (async ():Promise<void> => { await this.makeDir(this.valueMapDirPath)})(),
      (async ():Promise<void> => { await this.makeDir(this.keyVsTypeMapDirPath)})()
    ])
  }

  makeDir = async (dirPath: string) => {
    await this.fsPromises.mkdir(dirPath)
            .catch((e) => {
              if (e.code === `EEXIST`){
                console.log(`${dirPath} already exist`);
              } else {
                console.log(` ${e.code} in createDir`);
                throw (e)
              }
            })
  }

}

let makePlayerDir = new MakePlayerDir();
export { makePlayerDir };

*/