class MSWComposition {
  room = (): Array<string> => {
    let compositionRoom: Array<string> = [];
    compositionRoom.push(`id`);
    compositionRoom.push(`title`);
    compositionRoom.push(`description`);
    compositionRoom.push(`npc`);
    compositionRoom.push(`exit`);
    return compositionRoom;
  };

  resident = (): Array<string> => {
    let compositionResident: Array<string> = [];
    compositionResident.push(`id`);
    compositionResident.push(`name`);
    compositionResident.push(`location`);
    compositionResident.push(`inventory`);
    compositionResident.push(`attribute`);
    compositionResident.push(`purse`);
    return compositionResident;
  };
}

let mswComposition = new MSWComposition();
export { mswComposition };
