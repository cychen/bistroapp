

//import * as fs from "fs";
//import * as path from "path";
//import { makePlayerDir } from "./make-player-dir";


//class MSWDataDir {
//  fsPromises = fs.promises;
//  itemDataFilePath = path.join(__dirname, "../mswItemData");
//  roomDataFilePath = path.join(__dirname, "../mswRoomData");
//  npcDataFilePath = path.join(__dirname, "../mswNpcData");
//const ItemTextMapFilePath = `${ItemDataFilePath}/textMap`;
//const ItemValueMapFilePath = `${ItemDataFilePath}/valueMap`;
//const ItemBooleanMapFilePath = `${ItemDataFilePath}/booleanMap`;

//mswPlayer/textMap/name.json; location.json;
//                 <uuid, name>; <uuid, roomId>
//         /valueMap/money.json;
//                <uuid, number>
//         /keyVsTypeMap/keyVsType.json; 
//         <'name', 'text'>;<'location', 'text'>;<'money', 'value'>

//createPlayerDir: run only once to prepare data-dir;

/*
  playerDirPath = path.join(__dirname, "../mswPlayer");
  textMapDirPath = `${this.playerDirPath}/textMap`;
  valueMapDirPath = `${this.playerDirPath}/valueMap`;
  keyVsTypeMapDirPath = `${this.playerDirPath}/keyVsTypeMap`;

  keyVsTypeMap: Map<string, string> = new Map();
  storageTextMap: Map<string, Map<string, string>> = new Map();
  storageValueMap: Map<string, Map<string, number>> = new Map();

  defaultTextMapKeys:Array<string> = [`name`, `location`];
  defaultValueMapKeys:Array<string> = [`money`];
 

  newPlayerDir = async () => {
 //   console.log(`1`)
    await makePlayerDir.makeDirFamily();
  //  console.log(`6`)
   // this.createDefaultMapFile();
   // await this.saveDefaultMapFile();
  }  

 
  createDefaultMapFile = async () => {   
    this.createDefaultTextMap();
    this.createDefaultValueMap();
  }

  saveDefaultMapFile = async () => {
    await this.saveDefaultKeyVsTypeMapFile();
    await this.saveDefaultTextMapFile();
    await this.saveDefaultValueMapFile();
  }

  createDefaultTextMap = () => {
    for (let i=0; i<this.defaultTextMapKeys.length; i++){
      let mapKey: string = this.defaultTextMapKeys[i];
      let thisMap: Map<string, string> = new Map();
      this.keyVsTypeMap.set(`${mapKey}`, `text`);
      this.storageTextMap.set(`${mapKey}`, thisMap);
    }
  }

  createDefaultValueMap = () => {
    for (let i=0; i<this.defaultValueMapKeys.length; i++){
      let mapKey: string = this.defaultValueMapKeys[i];
      let thisMap: Map<string, number> = new Map();
      this.keyVsTypeMap.set(`${mapKey}`, `value`);
      this.storageValueMap.set(`${mapKey}`, thisMap);
    }
  }

  saveDefaultKeyVsTypeMapFile = async ()=> {
    let fileName: string = `keyVsTypeMap`;
    let fileType: string = `json`;
    let filePath: string = this.keyVsTypeMapDirPath + `/` + fileName + `.` + fileType;
    let fileData: string = JSON.stringify(Object.fromEntries(this.keyVsTypeMap));
    await this.writeFile(filePath, fileData);
  }

  saveDefaultTextMapFile = async ()=> {
    let fileType: string = `json`;
    for (let i=0; i<this.defaultTextMapKeys.length; i++){
      let mapName: string = this.defaultTextMapKeys[i];
      let thisMap: Map<string, string> = new Map(); 
      let filePath: string = this.textMapDirPath + `/` + mapName + `.` + fileType;
      let fileData: string = JSON.stringify(Object.fromEntries(thisMap));
      await this.writeFile(filePath, fileData);
    }
  }

  saveDefaultValueMapFile = async ()=> {
    console.log(`16`)
    let fileType: string = `json`;
    for (let i=0; i<this.defaultValueMapKeys.length; i++){
      let mapName: string = this.defaultValueMapKeys[i];
      let thisMap: Map<string, number> = new Map(); 
      let filePath: string = this.valueMapDirPath + `/` + mapName + `.` + fileType;
      let fileData: string = JSON.stringify(Object.fromEntries(thisMap));
      await this.writeFile(filePath, fileData);
    }
  }

  loadPlayerMap = async () => {
    let mapData: string = await this.loadKeyVsTypeMap();
    this.keyVsTypeMap = new Map(Object.entries(JSON.parse(mapData)));
  }

  loadKeyVsTypeMap = async () => {
    console.log(`1`);
    let fileType = `json`;
    let fileName: string = `keyVsTypeMap`;
    let filePath: string = this.keyVsTypeMapDirPath + `/` + fileName + `.` + fileType;
    return await this.fsPromises.readFile(filePath, `utf8`)
      .catch(async (e) => {
              console.log(`e:${e.code}; emessage: ${e.message}`);
              throw (e);             
      })
  }

  playerDir = async () => {
    console.log(`1`)
  //  this.defaultTextMap();
    await this.makePlayerDir();
    console.log('4')
    await this.makeMapDir();
    console.log(`8`)
    await this.readMapFile()
    console.log(`18`)
  }

  readMapFile = async () => {
    console.log('9')
    let keyVsTypeMapData = await this.readKeyVsTypeMapFile(this.keyVsTypeMapDirPath, `keyVsTypeMap`);
    console.log(`11`);
    let keyVsTypeMap:Map<string, string> = new Map(Object.entries(JSON.parse(keyVsTypeMapData)));
    let fileData: Promise<[string, string]> = Promise.all([
      (async ():Promise<string> => { return await this.readTextMapFile(this.textMapDirPath, `nameMap`);
                   })(),
      (async ():Promise<string> => { return await this.readTextMapFile(this.textMapDirPath, `locationMap`);
                    })()
    ])   
    let mapData = await fileData;
    console.log(`14`)
    console.log(`mapData length: ${mapData.length}`)
    let nameMapData= mapData[0];
    let nameMap:Map<string, string> = new Map();
    let locationMapData = mapData[1];
    console.log(`15; map1Data:${map1Data}`)
  }

  readKeyVsTypeMapFile = async (mapDir: string, fileName: string) => {
    console.log(`10`);
    let fileType = `json`;
    let thisFilePath = mapDir + `/` + fileName + `.` + fileType;
    return await this.fsPromises.readFile(thisFilePath, `utf8`)
      .catch(async (e) => {
              console.log(`e:${e.code}; emessage: ${e.message}`)
              if (e.code === `ENOENT`){
                this.createDefaultMap();
                let thisMap:Map<string, string> = this.keyVsTypeMap;
                let mapData = JSON.stringify(Object.fromEntries(thisMap));
                console.log(`13`)
                return mapData;
              } else {
                console.log(`e:${e.code}; emessage: ${e.message}`)
                throw (e);
              }
      })
  }

  oldreadMapFile = async () => {
    console.log('9')
    let fileData: Promise<[string, string]> = Promise.all([
      (async ():Promise<string> => { return await this.readTextMapFile(this.textMapDirPath, `nameMap`);
                   })(),
      (async ():Promise<string> => { return await this.readTextMapFile(this.textMapDirPath, `locationMap`);
                    })()
    ])   
    let mapData = await fileData;
    console.log(`14`)
    console.log(`mapData length: ${mapData.length}`)
    let nameMapData= mapData[0];
    let nameMap:Map<string, string> = new Map();
    let locationMapData = mapData[1];
    console.log(`15; map1Data:${map1Data}`)
  }

  readTextMapFile = async (mapDir: string, fileName: string) => {
    console.log(`10`);
    let fileType = `json`;
    let thisFilePath = mapDir + `/` + fileName + `.` + fileType;
    let filedata: string;
    return await this.fsPromises.readFile(thisFilePath, `utf8`)
      .catch(async (e) => {
                      console.log(`e:${e.code}; emessage: ${e.message}`)
              if (e.code === `ENOENT`){
                let thisMap:Map<string, string> = new Map();
                thisMap.set(`text`, `data`);
                filedata = JSON.stringify(Object.fromEntries(thisMap));
                await this.writeFile(thisFilePath, filedata);
                console.log(`13`)
                return filedata;
              } else {
                console.log(`e:${e.code}; emessage: ${e.message}`)
                throw (e);
              }
      })
  }

  writeFile = async (thisFilePath: string, data: string) => {
    await this.fsPromises.writeFile(thisFilePath, data, `utf8`)
        .catch((e) => {
          console.log(`${e.code} in writeFile`);
          throw (e)
        })    
  }


  createMapDir = async () => {
    console.log('5')
    //promise.all([promise1,promise2,..]).then(values:[value1,value2,..])
    await Promise.all([
      (async () => {
                     await this.createSpecificMapDir(this.textMapDirPath);
                   })(),
      (async () => {
                     await this.createSpecificMapDir(this.valueMapDirPath);
                   })(),
      (async () => {
                     await this.createSpecificMapDir(this.keyVsTypeMapDirPath);
                   })()               
      ]).catch((e)=>{
        console.log(`${e.code} in createMapDir`);
        throw (e);
      })
  }

  createSpecificMapDir = async (specificMapDirPath:string) => {
    console.log('6')
    await this.fsPromises.mkdir(specificMapDirPath)
            .catch((e) => {
              if (e.code === `EEXIST`){
                console.log(`${specificMapDirPath} directory already exist`);
              } else {
                console.log(`error:${e.code} in createSpecificMapDir`);
                throw (e)
              }
            })
    console.log('7')
  }
  */

//}


//let mswDataDir = new MSWDataDir();
//export { mswDataDir }
