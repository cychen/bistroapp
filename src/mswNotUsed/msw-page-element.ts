/*
import { styleLabel } from "./mswStyle/style-label";
import { ModelContainer } from "./mswModel/model-container";
import { ModelTextArea } from "./mswModel/model-textarea";
import { gameText } from "./msw-game-text";
import { styleTextarea } from "./mswStyle/style-textarea";
import { ModelBtn } from "./mswModel/model-btn";
import { ModelInput } from "./mswModel/model-input";
import { styleContainer } from "./mswStyle/style-container";
import { styleButton } from "./mswStyle/style-button";
import { gameStartBtnEvtLs } from "./msw-start-btn";

class MSWPageElement {
  
  inputElement = new ModelInput();
  gameStartBtn = new ModelBtn();
  pageTitleTextArea = new ModelTextArea();
  pageBodyTextArea = new ModelTextArea();
  pageContainer = new ModelContainer();
  pageTitleContainer = new ModelContainer();
  pageBodyContainer = new ModelContainer();
  pageInputContainer = new ModelContainer();
  pageBtnContainer = new ModelContainer();

  let createGameElements = ()=> {
    createGameTitleTextElement();
    createMainBodyTextElement();
    createLoginInputElement();
    createStartBtnElement();
  }

  let createLoginInputElement = () => {
    loginInputElement.setAttribute('maxlength', '8');
    loginInputElement.setAttribute('placeholder', 'enter character name to new/enter game');
    loginInputElement.setStyle(styleTextarea.loginInputStyle());
  }

  let createStartBtnElement = () => {   
    startBtn.setTextContent('start');
    startBtn.setStyle(styleButton.newBieBtnStyle());
    startBtn.node.addEventListener('click', () => {
      let inputNode = loginInputElement.node as HTMLInputElement;
      gameStartBtnEvtLs(startBtn, inputNode);
    })
  }
  
  let createGameTitleTextElement = () => {
    pageTitleTextArea.setTextContent(gameText.gameTitleText());
    pageTitleTextArea.setStyle(styleTextarea.gameTitleStyle());
  }

  let createMainBodyTextElement = () => {
    pageBodyTextArea.setTextContent(gameText.welcomeText());
    pageBodyTextArea.setStyle(styleTextarea.welcomeStyle());
  }

  let createPageContainers = () => {
    pageContainer.setStyle(styleContainer.pageContainer());
    pageTitleContainer.setStyle(styleContainer.titleContainer());
    pageBodyContainer.setStyle(styleContainer.mainBodyContainer());
    pageInputContainer.setStyle(styleContainer.inputContainer());
    pageBtnContainer.setStyle(styleContainer.gameBtnContainer());
  }

  let appendGameElementToContainer = () => {
    pageTitleContainer.node.appendChild(pageTitleTextArea.node);
    pageBodyContainer.node.appendChild(pageBodyTextArea.node);
    pageInputContainer.node.appendChild(loginInputElement.node);
    pageBtnContainer.node.appendChild(startBtn.node);
  }

  let assemblePageContainer = () => {
    pageContainer.node.appendChild(pageTitleContainer.node);
    pageContainer.node.appendChild(pageBodyContainer.node);
    pageContainer.node.appendChild(pageInputContainer.node);
    pageContainer.node.appendChild(pageBtnContainer.node);
    document.body.appendChild(pageContainer.node);
  }

  let setDocumentBodyStyle = () => {
    document.body.setAttribute("style", `${styleLabel.documentBodyStyle()}`);
  };

  let renderWelcomePage = ()=>{
    createGameElements();  
    createPageContainers();
    appendGameElementToContainer();
    assemblePageContainer();
    setDocumentBodyStyle();   
  };
  
}
export { renderWelcomePage };
*/