import { EventEmitter } from "events";
import * as fs from "fs";
import { v4 as uuidV4 } from "uuid";
import * as path from "path";

import { mswStorageDataMap } from "../msw-storage-datamap";

/*
residentComposition:[id,name,location,inventory,attribute,purse]
attribute: stamina:(韌性，持續性，耐力，精力，非生命力）endurance(str)

mswItem related data:
1.id
2.name
3.description
4.equipable(yesNo)? : yes => equipToBodyPart(head,arm)
5.wearAndTearable(yesNo)? : yes => wearAndTearDegree(10%),
6.ownerShip(ownedByWho),
7.whereItIs => npcEquipped, npcStore, playerEquipped, playerBag, playerInventory

player related data:
1.id
2.name
3.money
4.location -> map<playerUUID, $roomId>
5.equippedGears:
  a: check itemOwnerShip from map<itemId, ownerId>; return listOfItemId
  b: for item in listOfItemId, combine with "whereItIs => playerEquipped" to find playerEquippedGears
  c: for item in "playerEquippedGears", combine with "equipToBodyPart", to show playerEquipGears
6.itemsInPlayerBag:
  a: same as step a in "equippedGears"
  b: combine with "whereItIs => playerBag"
7.itemsInPlayerInventory:
  a: same as step a in "equippedGears"
  b: combine with "whereItIs" => playerInventory"


dataFile: playerData
1: "name" => map<uuid, name>; textMap
2: "money" => map<uuid, gold>; valueMap
3: "location" => map<uuid, roomId>; textMap

dataFile: mswItemData => give Each Item an Id
1: "name" => map<itemId, name>
2: "description" => map<itemId, description>
3: "equippable" => map<itemId, boolean>
4: "wearAndTearable" => map<itemId, boolean>
5: "stackable" => map<itemId, boolean>
6: "equipToBodyPart" = map<itemId, bodypart>
7: "wearAndTearDegree" => map<itemId, wearAndTearPercent>
8: "maxStackQty" => map<itemId, qty>
9: "ownership" => map<itemId, ownerId>
10: "whereItIs" => map<itemId, itemLocation>
11: "quantity" => map<itemId, qty>

room related data:
1.name -> map<roomId, roomName>
2.description -> map<roomId, roomDescription>
3.roomExit: map<roomId, map<exitDirection, exitToRoomId>>
  => map<roomId, exitId> + map<exitId, exitDirection> + map<exitId, exitToRoomId>
  a.create map<exitId, presentRoomId>
  b.create map<exitId, exitToRoomId>
  c.create map<exitId, direction(newsdu)>

npc related data:
1."name" -> map<npcId,npcName>
2."description" -> map<npcId, npcDescription>
3."location" -> map<npcId, roomId>

dataMap: textMap, valueMap, booleanMap
storageTextMap: Map<string, Map<string, string>> = new Map();
storageValueMap: Map<string, Map<string, number>> = new Map();
storageBooleanMap: Map<string, Map<string, boolean>> = new Map();

*/

class MSWResidentMap {
  roomTitleMap: Map<string, string> = new Map();
  roomDescriptionMap: Map<string, string> = new Map();
  roomNpcMap: Map<string, Array<string>> = new Map();
  roomExitMap: Map<string, Array<string>> = new Map();

  addRoom = (
    roomTitle: string,
    roomDescription: string,
    npc: Array<string>,
    exit: Array<string>,
  ) => {
    let roomUUID = uuidV4();
    this.roomTitleMap.set(roomUUID, roomTitle);
  };

  getRoomTextMapFromMswDataMap = () => {
  //  mswDataMap.getTextMap(`roomTitle`);
  };
}

let mswResidentMap = new MSWResidentMap();
export { mswResidentMap };
