import { EventEmitter } from "events";
import { v4 as uuidV4 } from "uuid";
import * as fs from "fs";
import * as path from "path";

import { mswStorageDataMap } from "../msw-storage-datamap";

/*
dataMap:
storageTextMap: Map<string, Map<string, string>> = new Map();
  roomTitleMap = <'roomTitle', map<roomId, roomTitle>>
  roomDescriptionMap =<'roomDescription', map<roomId, roomDescription>>

storageArrayTextMap: Map<string, map<string, array<string>>
  roomNpcMap = <'npc', map<roomId, array<npcId>>
  roomExitMap =<'exit', map<roomId, array<exit>>

roomMap has two types:
  1:textMap<string, string>
  2:arrayTextMap<string, array<string>>
  type 1: roomTitle & roomDescription
    roomTitleMap = map<roomId, roomTitle>
    roomDescriptionMap = map<roomId, roomDescription>
  type 2: roomNpc & roomExit
    roomNpcMap = map<roomId, array<npcId>>>
    roomExitMap: map<roomId, array<exit>>

*/

class MSWRoomMap {
  roomTitleMap: Map<string, string> = new Map();
  roomDescriptionMap: Map<string, string> = new Map();
  roomNpcMap: Map<string, Array<string>> = new Map();
  roomExitMap: Map<string, Array<string>> = new Map();

  addRoom = (
    roomTitle: string,
    roomDescription: string,
    npc: Array<string>,
    exit: Array<string>,
  ) => {
    let roomUUID = uuidV4();
    this.roomTitleMap.set(roomUUID, roomTitle);
  };

  getRoomTextMapFromMswDataMap = () => {
   // mswDataMap.getTextMap(`roomTitle`);
  };
}

let mswRoomMap = new MSWRoomMap();
export { mswRoomMap };
