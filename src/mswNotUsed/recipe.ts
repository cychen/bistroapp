/*
import * as uuidV4 from 'uuid/v4';
import { dataMap } from './data-map';

let recipeIdToNameMap = new Map();
let foodIdToNameMap = new Map();
let ingredientIdToNameMap = new Map();
let condimentIdToNameMap = new Map();

let ingredientIdToCategoryMap: Map<string, string> = new Map();
let condimentIdToCategoryMap: Map<string, string> = new Map();

let potatoRecipeIdToMaterialQtyMap: Map<string, number> = new Map()
let saltRecipeIdToMaterialQtyMap: Map<string, number> = new Map();
let oilRecipeIdToMaterialQtyMap: Map<string, number> = new Map();

let bakePotato = () => {

  let food = 'bakePotato';
  let foodId = uuidV4();
  let recipeId = uuidV4();

  recipeIdToNameMap.set(recipeId, food);
  foodIdToNameMap.set(foodId, food);

  let ingredientName = 'potato';
  let ingredientId = uuidV4();
  ingredientIdToNameMap.set(ingredientId, ingredientName);

  let ingredientCategory = 'primary';
  ingredientIdToCategoryMap.set(ingredientId, ingredientCategory);

  let ingredientQty = 1;
  potatoRecipeIdToMaterialQtyMap.set(recipeId, ingredientQty);

  let condiment1Name = 'salt';
  let condiment1Id = uuidV4();
  condimentIdToNameMap.set(condiment1Id, condiment1Name);

  let condiment1Category = 'basic';
  condimentIdToCategoryMap.set(condiment1Id, condiment1Category);

  let condiment1Qty = 1;
  saltRecipeIdToMaterialQtyMap.set(recipeId, condiment1Qty);

  let condiment2Name = 'oil';
  let condiment2Id = uuidV4();
  condimentIdToNameMap.set(condiment2Id, condiment2Name);

  let condiment2Category = 'basic';
  condimentIdToCategoryMap.set(condiment2Id, condiment2Category);

  let condiment2Qty = 1;
  oilRecipeIdToMaterialQtyMap.set(condiment2Id, condiment2Qty);





}

*/
