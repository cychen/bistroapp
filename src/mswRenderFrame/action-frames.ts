import { UIElement } from "../ui-element";

class ActionFrames {
  inputSection: UIElement;
  actMenuSection: UIElement;

 constructor (){
   this.inputSection = new UIElement('div')
       .setStyle('flex-container');
   this.actMenuSection = new UIElement('div')
       .setStyle('no-flex-container'); 

 } 

}

let actionFrames = new ActionFrames();
export { actionFrames } 