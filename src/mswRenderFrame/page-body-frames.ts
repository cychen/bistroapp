import { UIElement } from "../ui-element";

class PageBodyFrames {
  gameRoomSection: UIElement;
  gameActionSection: UIElement;

 constructor (){
   this.gameRoomSection = new UIElement('section')
       .setStyle('flex-column');
   this.gameActionSection = new UIElement('section')
       .setStyle('no-flex-container'); 

 } 

}

let pageBodyFrames = new PageBodyFrames();
export { pageBodyFrames } 