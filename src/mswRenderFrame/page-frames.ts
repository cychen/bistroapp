import { UIElement } from "../ui-element";

class PageFrames {
   mainPage: UIElement;
   pageHeadSection: UIElement;
   pageBodySection : UIElement;

  constructor (){
    this.mainPage = new UIElement('section')
        .setStyle('flex-column');
    this.pageHeadSection = new UIElement('section')
        .setStyle('no-flex-container'); 
    this.pageBodySection = new UIElement('section')
        .setStyle('flex-column');
   // this.mainPage.node.appendChild(this.pageHeadSection.node);
   // this.mainPage.node.appendChild(this.pageBodySection.node);
  } 

}

let pageFrames = new PageFrames();
export { pageFrames } 