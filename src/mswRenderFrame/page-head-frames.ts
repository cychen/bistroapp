import { UIElement } from "../ui-element";

class PageHeadFrames {
  gameTitleSection: UIElement;
  gameSubTitleSection: UIElement;
  gameMenuSection : UIElement;

 constructor (){
   this.gameTitleSection = new UIElement('section')
       .setStyle('flex-container');
   this.gameSubTitleSection = new UIElement('section')
       .setStyle('flex-container'); 
   this.gameMenuSection = new UIElement('section')
       .setStyle('flex-container');

 } 

}

let pageHeadFrames = new PageHeadFrames();
export { pageHeadFrames } 