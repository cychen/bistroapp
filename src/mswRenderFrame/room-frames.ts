import { UIElement } from "../ui-element";

class RoomFrames {
  roomTitleSection: UIElement;
  roomBodySection: UIElement;

 constructor (){
   this.roomTitleSection = new UIElement('section')
       .setStyle('no-flex-container');
   this.roomBodySection = new UIElement('section')
       .setStyle('flex-column'); 

 } 

}

let roomFrames = new RoomFrames();
export { roomFrames } 