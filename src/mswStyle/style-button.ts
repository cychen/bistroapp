
class StyleButton {
 gameStartChoiceBtnStyle = (): string => {
    return (`
    flex: 1 0 auto;
  /*  font-style: italic; */
    text-align: center;
    font-size: 1rem;
    color: #fff;
    text-shadow: 1px 1px 1px #000;
    background-color: rgba(220, 0, 0, 1);
    background-image: linear-gradient(to top left,
      rgba(0, 0, 0, 0.2),
      rgba(0, 0, 0, 0.2) 30%,
      rgba(0, 0, 0, 0));
    box-shadow: inset 2px 2px 3px rgba(255, 255, 255, 0.6),
    inset -2px -2px 3px rgba(0, 0, 0, 0.6);
/*    width: 6rem; */
  /*  height: 2.2rem; */
   /* margin-top: 2rem; */
  /*  margin-left: auto; */
  /*  margin-right: auto; */
   /* margin-bottom: 2rem; */
    border-radius: 10px;
    margin: 2px 5px;

  `);
  }
 actionBtnStyle = (): string => {
    return (`
    flex: 1 0 auto;
  /*  font-style: italic; */
  /*  text-align: center; */
    font-size: 1rem;
    color: #fff;
    text-shadow: 1px 1px 1px #000;
    background-color: rgba(220, 0, 0, 1);
    background-image: linear-gradient(to top left,
      rgba(0, 0, 0, 0.2),
      rgba(0, 0, 0, 0.2) 30%,
      rgba(0, 0, 0, 0));
    box-shadow: inset 2px 2px 3px rgba(255, 255, 255, 0.6),
    inset -2px -2px 3px rgba(0, 0, 0, 0.6);
 /*   width: 6rem; */
 /*   height: 2.2rem; */
    border-radius: 10px;
    margin: 2px 5px;

  `);
  }
disabledBtnStyle = (): string => {
    return (`
    flex: 1 0 auto;
  /*  font-style: italic; */
   text-align: center;
   font-size: 1rem;
    color: rgba(220,0,0,1);
    text-shadow: 1px 1px 1px #000;
    background-color: rgba(220, 0, 0, 1);
    background-image: linear-gradient(to top left,
      rgba(0, 0, 0, 0.2),
      rgba(0, 0, 0, 0.2) 30%,
      rgba(0, 0, 0, 0));
    box-shadow: inset 2px 2px 3px rgba(255, 255, 255, 0.6),
    inset -2px -2px 3px rgba(0, 0, 0, 0.6);
  /*  width: 6rem; */
  /*  height: 2.2rem; */
    border-radius: 10px;
    margin: 2px 5px;
  `);
  }
  residentBtnStyle = (): string => {
    return (`
  /*  font-style: italic; */
    text-align: center;
    font-size: 1rem;
    color: #fff;
    text-shadow: 1px 1px 1px #000;
    background-color: rgba(220, 0, 0, 1);
    background-image: linear-gradient(to top left,
      rgba(0, 0, 0, 0.2),
      rgba(0, 0, 0, 0.2) 30%,
      rgba(0, 0, 0, 0));
    box-shadow: inset 2px 2px 3px rgba(255, 255, 255, 0.6),
    inset -2px -2px 3px rgba(0, 0, 0, 0.6);
    width: 6rem;
    height: 2.2rem;;
    border-radius: 10px;
  `);
  }

backBtnStyle = (): string => {
    return (`
    flex: 1 0 auto;
  /*  font-style: italic; */
    text-align: center;
    font-size: 1rem;
    color: #fff;
    text-shadow: 1px 1px 1px #000;
    background-color: rgba(220, 0, 0, 1);
    background-image: linear-gradient(to top left,
      rgba(0, 0, 0, 0.2),
      rgba(0, 0, 0, 0.2) 30%,
      rgba(0, 0, 0, 0));
    box-shadow: inset 2px 2px 3px rgba(255, 255, 255, 0.6),
    inset -2px -2px 3px rgba(0, 0, 0, 0.6);
/*    width: 6rem; */
/*    height: 2.2rem; */
    margin-top: 1rem;
    margin-left: auto;
    margin-right: auto;
    margin-bottom: 2rem;
    border-radius: 10px;
  `);
  }

  btnHoverStyle = (): string => {
    return (`
    background-color: rgba(255, 0, 0, 1);
    `);
  }

  btnActiveStyle = (): string => {
    return (`
    box-shadow: inset -2px -2px 3px rgba(255, 255, 255, 0.6),
    inset 2px 2px 3px rgba(0, 0, 0, 0.6)`);
  }



}

let styleButton = new StyleButton();
export { styleButton }
