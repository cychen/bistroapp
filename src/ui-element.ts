class UIElement {
    readonly node: HTMLElement;
// stylesheet class or className usage:
// o.node.setAttribute('class', 'nametoset');
// or o.node.className = nametoset; 

    constructor(elementTagName: string) {
        this.node = document.createElement(elementTagName);
    }

    setTextContent = (text: string): UIElement => {
        this.node.textContent = text;
        return this;
    }

    setAttribute = (attr: string, value: any): UIElement => {
        this.node.setAttribute(attr, value);
        return this;
    }

    removeAttribute = (attr: string): UIElement => {
        this.node.removeAttribute(attr);
        return this;
    }

    setStyle = (className: string): UIElement => {
        this.node.className = className;
        return this;
    }

  

    /*
    setStyle = (cssStyle: string): UIElement => {
        this.node.className = rID();
        let style = document.createElement('style');
        style.textContent = `.${this.node.className}{${cssStyle}}`;
        document.head.appendChild(style);
        return this;
    }
*/
    appendToParent = (parent: HTMLElement): UIElement => {
        parent.appendChild(this.node);
        return this;
    }

}

let rID = (): string => {
    let hexId = 'UI-'
    for (let i = 0; i < 4; i++) {
        let rIndex = Math.floor(Math.random() * 16)
        hexId += '0123456789ABCDEF'[rIndex]
    }
    return hexId
}


export { UIElement }
